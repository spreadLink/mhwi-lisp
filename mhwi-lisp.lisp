;;;; mhwi-lisp.lisp

(in-package #:mhwi-lisp)



(defun init-dbs ()
  (init-weapons)
  (init-skills)
  (init-decorations)
  (init-charms)
  (init-armour))



;;; EFR is calculated with the formula:
(defun efr (true-raw crit-mod sharpness)
  (* true-raw crit-mod sharpness))

;;; the true-raw value is the attack value divided by the bloat value

;;; the crit-mod is calculated with the formula:
;;; (1+ (* affinity crit-dmg))
;; where
(defun crit-dmg (crit-boost)
  (case crit-boost
    (0 0.25)
    (1 0.30)
    (2 0.35)
    (3 0.40)))

;; the sharpness is
(defun sharpness (colour)
  (case colour
    (:red 0.5)
    (:orange 0.75)
    (:yellow 1)
    (:green 1.05)
    (:blue 1.2)
    (:white 1.32)))


(defun set-efr (weapon crit-boost)
  (with-slots (attack (wpn-class class) affinity max-sharpness) weapon
    (let ((true-efr (/ attack (bloat-value wpn-class)))
          (crit-mod (1+ (* (crit-dmg crit-boost) affinity)))))))

