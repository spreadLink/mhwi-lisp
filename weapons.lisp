(in-package :mhwi-lisp)


(defstruct weapon
  name 
  class
  rarity 
  attack  
  affinity 
  slots  
  rawp  
  max-sharpness
  aug-scheme 
  upgrade-scheme)

(defvar *weapons*)

(defun bloat-value (weapon-class)
  (case weapon-class
    (:greatsword 4.8)
    ((:sns :dual-blades) 1.4)
    (:longsword 3.3)
    (:hammer 5.2)
    (:hunting-horn 4.2)
    ((:lance :gunlance) 2.3)
    (:switch-axe 3.5)
    (:charge-blade 3.6)
    (:insect-glaive 3.1)
    (:bow 1.2)
    (:heavy-bowgun 1.5)
    (:light-bowgun 1.3)))

(defun init-weapons ()
  (let ((weapons  (with-open-file (in (data-file "database_weapons.json"))
                     (cl-json:decode-json in))))
    (setf *weapons*  (make-array (length weapons) :fill-pointer 0))
    (loop :for (nil . data) :in weapons :do
          (vector-push (json-to-struct (data make-weapon)
                         :name
                         (:class :class (lispy))
                         :rarity
                         :attack
                         :affinity
                         :slots
                         (:rawp :is-raw)
                         (:max-sharpness :maximum-sharpness)
                         (:aug-scheme :augmentation-scheme (lispy))
                         (:upgrade-scheme :> (let ((scm (assocdr :upgrade-scheme data)))
                                               (if (string= scm "NONE")
                                                   nil
                                                   (lispy scm)))))
                       *weapons*))))
