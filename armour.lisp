(in-package :mhwi-lisp)


(defstruct armour-set
  name
  discriminator
  rarity
  prefix
  bonus)

(defstruct armour-piece
  set
  variant
  slots
  skills)

(defvar *heads*)
(defvar *chests*)
(defvar *arms*)
(defvar *waists*)
(defvar *legs*)

(defparameter *valid-variants*
  '(:hr-alpha :hr-beta :hr-gamma
    :mr-alpha-plus :mr-beta-plus))


(defun armour-piece (set variant piece)
  (destructuring-bind (fst snd) piece
    (make-armour-piece
     :set set
     :variant variant
     :slots fst
     :skills (mapcar (lambda (e)
                       (destructuring-bind (head . tail) e
                         (cons (gethash head *skill-ids*)
                               tail)))
                     snd))))

(defun init-armour ()
  (with-open-file (in (data-file "database_armour.json"))
    (let* ((json (cl-json:decode-json in))
           (armours (assocdr :armour json))
           (armour-len (length armours)))

      (setf *heads* (make-array armour-len :fill-pointer 0)
            *chests* (make-array armour-len :fill-pointer 0)
            *arms* (make-array armour-len :fill-pointer 0)
            *waists* (make-array armour-len :fill-pointer 0)
            *legs* (make-array armour-len :fill-pointer 0))
      
      (dolist (data armours)
        (let ((set (json-to-struct (data make-armour-set)
                     (:name :set)
                     (:discriminator :> (pipe (assocdr :discriminator data)
                                          (js-to-lisp :$)
                                          (intern :$ "KEYWORD")))
                     :rarity
                     :prefix
                     (:bonus :> (pipe (assocdr :set-bonus data)
                                  (js-to-lisp :$)
                                  (intern :$ "KEYWORD")
                                  (gethash :$ *set-bonus-ids*))))))
          
          (dolist (variant *valid-variants*)
            (let ((var (assocdr variant data)))
              (when var
                (let ((head (assocdr :head var)))
                  (when head (vector-push (armour-piece set variant head) *heads*)))
                (vector-push (armour-piece set variant (assocdr :chest var)) *chests*)
                (vector-push (armour-piece set variant (assocdr :arms var)) *arms*)
                (vector-push (armour-piece set variant (assocdr :waist var)) *waists*)
                (vector-push (armour-piece set variant (assocdr :legs var)) *legs*)))))))))
