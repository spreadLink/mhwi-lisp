(in-package :mhwi-lisp)

(defstruct decoration
  name
  slot
  skill)

(defparameter *decorations* (make-hash-table))

(defun init-decorations ()
  (with-open-file (in (data-file "database_decorations.json"))
    (let* ((json (cl-json:decode-json in))
           (decorations (assocdr :simple-decorations json))
           (4slot-combos (assocdr :4slot-compound-decorations-batch-definitions json))
           (4slot-singles (assocdr :4slot-single-skill-decorations json)))
      (loop :for (id . data) :in decorations :do
            (let ((decoration (json-to-struct (data make-decoration)
                                :name
                                :slot
                                (:skill :> (pipe (assocdr :skill data)
                                             (lispy :$)
                                             (gethash :$ *skill-ids*)
                                             (cons :$ 1)
                                             (list :$))))))
              (setf (gethash id *decorations*) decoration)))
      (loop :for (nil . combos) :in 4slot-combos :do
            (dolist (left (assocdr :left-side combos))
              (dolist (right (assocdr :right-side combos))
                (let ((l (gethash (lispy left) *decorations*))
                      (r (gethash (lispy right) *decorations*)))
                  (with-slots ((ln name) (ls skill)) l
                    (with-slots ((rn name) (rs skill)) r
                      (let ((new (make-decoration
                                  :name (format nil "~a/~a Jewel 4" ln rn)
                                  :slot 4
                                  :skill (append ls rs))))
                        (pipe (format nil "compound-~a-~a" l r)
                          (intern :$ "KEYWORD")
                          (gethash :$ *decorations*)
                          (setf :$ new)))))))))
      (loop :for (id . versions) :in 4slot-singles :do
            (dolist (v versions)
              (case v
                (2 (with-slots (name skill) (gethash id *decorations*)
                     (let ((new (make-decoration
                                 :name (format nil "~a Jewel+ 4" name)
                                 :slot 4
                                 :skill (list (cons (caar skill) 2)))))
                       (vector-push-extend new *decorations*)
                       (pipe (format nil "~a-x2" id)
                         (lispy :$)
                         (gethash :$ *decorations*)
                         (setf :$ new)))))
                (3 (with-slots (name skill) (gethash id *decorations*)
                     (let ((new (make-decoration
                                 :name (format nil "Hard ~a Jewel 4" name)
                                 :slot 4
                                 :skill (list (cons (caar skill) 3)))))
                       (vector-push-extend new *decorations*)
                       (pipe (format nil "~a-x3" id)
                         (lispy :$)
                         (gethash :$ *decorations*)
                         (setf :$ new)))))))))))





