(in-package :mhwi-lisp)

(defparameter *skills* (make-hash-table :test 'eq))
(defparameter *set-bonuses* (make-hash-table :test 'eq))

(defstruct skill
  name
  limit
  tooltip
  extended-limit
  states
  state-zero-p
  info 
  previous-name
  only-set-p)

(defstruct set-bonus
  name
  stages)


(defparameter *attack-boost* #(0 3 6 9 12 15 18 21))
(defun n-add-attack-boost (weapon ab-level)
  (with-slots (affinity attack) weapon
    (setf attack (+ attack (aref *attack-boost* ab-level)))
    (when (> ab-level 4)
      (setf affinity (+ affinity 5)))))

(defparameter *crit-eye* #(0 5 10 15 20 25 30 40))
(defun n-add-critical-eye (weapon ce-level)
  (setf (weapon-affinity weapon)
        (+ (weapon-affinity weapon) (aref *crit-eye* ce-level))))

(defparameter *raw-blunder* 0.75
  "crit-modifier with negative affinity")

(defparameter *crit-boost* #(1.25 1.30 1.35 1.40))

(defparameter *weakness-exploit* #(0 10 15 30))
(defparameter *weakness-exploit-wounded* #(0 5 15 20))
(defun n-add-weakness-exploit (weapon we-level)
  (setf (weapon-affinity weapon) (+ (weapon-affinity weapon)
                                    (aref *weakness-exploit* we-level)
                                    (aref *weakness-exploit-wounded* we-level))))

(defparameter *agitator-atk* #(0 4 8 12 16 20 24 28))
(defparameter *agitator-aff* #(0 5 5  7  7 10 15 20))
(defun n-add-agitator (weapon ag-level)
  (with-slots (affinity attack) weapon
      (setf attack (+ attack (aref *agitator-atk* ag-level))
            affinity (+ affinity (aref *agitator-aff* ag-level)))))


(defparameter *peak-performance* #(0 5 10 20))
(defun n-add-peak-performance (weapon pp-level)
  (setf (weapon-attack weapon) (+ (weapon-attack weapon)
                                  (aref *peak-performance* pp-level))))



(defun init-skills ()
  (with-open-file (in (data-file "database_skills.json"))
    (let* ((json (cl-json:decode-json in))
           (skills (rest (first json)))
           (set-bonuses (rest (second json))))
      (loop :for (id . data) :in skills :do
            (let ((skill (json-to-struct (data make-skill)
                           :name
                           :limit
                           :tooltip
                           (:extended-limit :extended-limit (or 0))
                           :states
                           (:state-zero-p :zeroth-state-can-be-blank)
                           :previous-name
                           (:only-set-p :only-from-set-bonuses))))
              (setf (gethash id *skills*) skill)))
      (loop :for (id . data) :in set-bonuses :do
            (let ((set-bonus (make-set-bonus
                              :name (assocdr :name data)
                              :stages (mapcar (lambda (e) (cons (cdar e) (cdadr e)))
                                              (assocdr :stages data)))))
              (setf (gethash id *set-bonuses*) set-bonus))))))



