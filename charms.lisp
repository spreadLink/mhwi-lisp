(in-package :mhwi-lisp)

(defstruct charm
  name
  max-level
  skills)


(defvar *charms*)

(defun init-charms ()
  (with-open-file (in (data-file "database_charms.json"))
    (let ((charms (assocdr :charms (cl-json:decode-json in))))
      (setf *charms* (make-array (length charms) :fill-pointer 0))
      (loop :for (ignore . data) :in charms :do
            (vector-push (json-to-struct (data make-charm)
                           :name
                           :max-level
                           (:skills :> (mapcar (lambda (e)
                                                 (pipe e
                                                   (js-to-lisp :$)
                                                   (intern :$ "KEYWORD")
                                                   (gethash :$ *skill-ids*)))
                                               (assocdr :skills data))))
                         *charms*)))))
