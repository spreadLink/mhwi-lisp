(in-package ::mhwi-lisp)


(defvar *data-path* (asdf:system-relative-pathname :mhwi-lisp "data/"))

(defun data-file (filename)
  (merge-pathnames filename *data-path*))

(defun assocdr (key alist)
  (cdr (assoc key alist)))


(defun js-to-lisp (string)
  (map 'string
       (lambda (c)
         (cond ((char= c #\_) #\-)
               ((lower-case-p c) (char-upcase c))
               (t c)))
       string))



(setf cl-json:*json-identifier-name-to-lisp* 'js-to-lisp)



(defmacro json-to-struct ((json constructor) &body key-slot-pairs)
  (flet ((convert (a)
           (if (atom a)
               `(,a (assocdr ,a ,json))
               (destructuring-bind (slot &optional key action) a
                 (let ((acc `(assocdr ,key ,json)))
                   (cond (action (case key
                                   (:> `(,slot ,action))
                                   (otherwise `(,slot (,(car action) ,acc ,@(cdr action))))))
                         
                         (key `(,slot ,acc))
                         
                         (t `(,slot (assocdr ,slot ,json)))))))))
    `(,constructor
      ,@(reduce (lambda (a b) (append (convert a) b)) key-slot-pairs
         :from-end t
         :initial-value nil))))



(defmacro pipe (item &body calls)
  (reduce (lambda (a b) (substitute a :$ b))
          calls
          :initial-value item))


(defun lispy (string)
  (pipe string
    (js-to-lisp :$)
    (intern :$ "KEYWORD")))
