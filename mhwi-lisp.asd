;;;; mhwi-lisp.asd

(asdf:defsystem #:mhwi-lisp
  :description "Port of simshadows MHWI build optimization tool"
  :author "Mia Kathage <siboru@googlemail.com>"
  :license  "MIT"
  :version "0.0.1"
  :serial t
  :depends-on (:cl-json)
  :components ((:file "package")
               (:file "util")
               (:file "skills")
               (:file "decorations")
               (:file "charms")
               (:file "armour")
               (:file "weapons")
               (:file "mhwi-lisp")))
